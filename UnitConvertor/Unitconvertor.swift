//
//  Unitconvertor.swift
//  UnitConvertor
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 pnut production. All rights reserved.
//

import Foundation

class UnitConvertor
{
    func degreeFahrenheit(degreesCelsius: Int) -> Int
    {
        return Int(1.8 * Float(degreesCelsius) + 32.0)
    }
}