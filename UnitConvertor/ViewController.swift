//
//  ViewController.swift
//  UnitConvertor
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 pnut production. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate
{

    let userDefaultsLastRowKey = "defaultCelsiusPickerRow"
    
    @IBOutlet var temperatureRange: TemperatureRange!
    
    private let convertor = UnitConvertor()
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var celsiusPicker: UIPickerView!
    
    /**
     Set the initial row from last known row index or return the default.
    */
    
    func initialPickerRow() -> Int
    {
        let savedRow = NSUserDefaults.standardUserDefaults().objectForKey(userDefaultsLastRowKey) as? Int
        
        if let row = savedRow
        {
            return row
        }
        else
        {
            return celsiusPicker.numberOfRowsInComponent(0)/2
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let row = initialPickerRow()
        celsiusPicker.selectRow(row, inComponent: 0, animated: false)
        pickerView(celsiusPicker, didSelectRow: row, inComponent: 0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let celciusValue = temperatureRange.values[row]
        return"\(celciusValue)℃"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // convert and display temperature
        //let degreesFahrenheit = 1.8 * degreesCelcius + 32.0
        
        displayConvertedTemperatureForRow(row)
        saveSelectedRow(row)
    }

    func saveSelectedRow(row: Int)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(row, forKey: "defaultCelsiusPickerRow")
        defaults.synchronize()
    }
    
    func displayConvertedTemperatureForRow(row: Int)
    {
        //let defaults = NSUserDefaults.standardUserDefaults()
        let degreesCelcius = Int(temperatureRange.values[row])
        temperatureLabel.text = "\(Int(convertor.degreeFahrenheit(degreesCelcius)))℉"
    }
    
    
}

