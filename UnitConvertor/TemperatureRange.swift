//
//  TemperatureRange.swift
//  UnitConvertor
//
//  Created by GDD Student on 10/5/16.
//  Copyright © 2016 pnut production. All rights reserved.
//

import Foundation
import UIKit

class TemperatureRange: NSObject, UIPickerViewDataSource
{
   let values = (-100...100).map {$0}
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
    return values.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    

}